﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Timers;
using System.Web.Http;
using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using TweetAnalyzerServer.Models.DTOs;
using JsonSerializer = Newtonsoft.Json.JsonSerializer;
using Tweet = TweetAnalyzerServer.Models.Tweet;

namespace TweetAnalyzerServer.Controllers
{
    public class TweetsController : ApiController
    {
        private const string url = "http://34.67.195.184/";
        private static double _tweetReadingInterval = 30000;

        // Creating a lazy loaded list of Tweets to avoid redundant requested to the
        // Twitter API before the first time data is consumed by the client
        private static Lazy<List<Tweet>> _tweets = new Lazy<List<Tweet>>(InitializeStream);

        // Resets the lazy loaded list of Tweets
        // Once list is accessed again, InitializeStream will be called and will read from the Twitter stream
        [Route("api/tweets/RefreshStream")]
        [HttpGet]
        public void RefreshStream()
        {
            _tweets = new Lazy<List<Tweet>>(InitializeStream);
        }

        // Adding the option to receive a user defined interval for reading the Twitter stream
        [Route("api/tweets/RefreshStream")]
        [HttpGet]
        public void RefreshStream(double interval)
        {
            _tweetReadingInterval = interval;
            RefreshStream();
        }

        // Reads tweets from the Twitter Streaming API for a user defined/default interval of time
        private static List<Tweet> InitializeStream()
        {
            var tweets = new List<Tweet>();
            JsonTextReader reader;
            using (var client = new WebClient())
            using (var stream = client.OpenRead(url))
            using (var streamReader = new StreamReader(stream))
            using (reader = new JsonTextReader(streamReader))
            {
                reader.SupportMultipleContent = true;
                reader.CloseInput = false;

                // Using a timer in order to limit the stream reading interval and interrupt when time elapses.
                var timer = new Timer(_tweetReadingInterval);

                // Subscribing a delegate to be called when timer elapses
                timer.Elapsed += Timer_Elapsed;

                // Starting timer right before starting to read data
                timer.Start();

                var serializer = new JsonSerializer();
                
                // Reading the stream and deserializing data to Tweets objects
                while (timer.Enabled && reader.Read())
                {
                    if (reader.TokenType == JsonToken.StartObject)
                    {
                        tweets.Add(serializer.Deserialize<Tweet>(reader));
                    }
                }
            }

            return tweets;
        }

        /// Occurs when the defined time interval for the stream elapses
        private static void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            ((Timer)sender).Stop();
        }

        [Route("api/tweets/GetTop10Words")]
        [HttpGet]
        public IEnumerable<WordDTO> GetTop10Words()
        {
            // Appending all text from all gathered tweets, trimming all punctuation symbols and
            // ordering by the frequency of occurrences of each word.
            var allText = new StringBuilder();
            _tweets.Value.ForEach(x => allText.Append(x.text).Append(' '));
            var punctuation = allText.ToString().Where(char.IsPunctuation).Distinct().ToArray();

            // Sanitizing all words from punctuation symbols and converting to lower case to prevent case sensitive errors
            var finalText = allText.ToString().ToLower().Trim(punctuation);
            var top10 = finalText
                .Split(' ')
                .GroupBy(x => x)
                .OrderByDescending(x => x.Count())
                .Take(10);

            // Converting result to DTO objects to send only relevant data
            return top10.Select(word => new WordDTO {Word = word.First(), Occurrences = word.Count()});
        }

        // Additional method for when the client wishes to restart the stream with a new interval time window
        [Route("api/tweets/GetTop10Words")]
        [HttpGet]
        public IEnumerable<WordDTO> GetTop10Words(bool restartStream, double interval)
        {
            if (restartStream)
            {
                RefreshStream(interval);
            }

            return GetTop10Words();
        }

        // Additional method for when the client wishes to restart the stream with the default interval time window
        [Route("api/tweets/GetTop10Words")]
        [HttpGet]
        public IEnumerable<WordDTO> GetTop10Words(bool restartStream)
        {
            return GetTop10Words(restartStream, _tweetReadingInterval);
        }

        [Route("api/tweets/GetTop10Users")]
        [HttpGet]
        public IEnumerable<UserDTO> GetTop10Users()
        {
            // Apparently several tweets are not attached to a user(User field is null) so filtering possible occurrences.
            var top10Users = _tweets.Value.Where(x => x.user != null).DistinctBy(x => x.user.id)
                .OrderByDescending(x => x.user.statuses_count)
                .Take(10).Select(x => x.user);

            // Converting result to DTO objects to send only relevant data
            return top10Users.Select(user => new UserDTO {ScreenName = user.screen_name, StatusCount = user.statuses_count});
        }

        // Additional method for when the client wishes to restart the stream with a new interval time window
        [Route("api/tweets/GetTop10Users")]
        [HttpGet]
        public IEnumerable<UserDTO> GetTop10Users(bool restartStream, double interval)
        {
            if (restartStream)
            {
                RefreshStream(interval);
            }

            return GetTop10Users();
        }

        // Additional method for when the client wishes to restart the stream with the default interval time window
        [Route("api/tweets/GetTop10Users")]
        [HttpGet]
        public IEnumerable<UserDTO> GetTop10Users(bool restartStream)
        {
            return GetTop10Users(restartStream, _tweetReadingInterval);
        }


        [Route("api/tweets/GetTop10Hashtags")]
        [HttpGet]
        public IEnumerable<HashtagDTO> GetTop10Hashtags()
        {
            var hashtagFrequencies = new Dictionary<string, int>();

            // Iterates all hashtags and counts their occurrences
            foreach (var tweet in _tweets.Value.Where(x => x.entities != null))
            {
                foreach (var hashtag in tweet.entities.hashtags)
                {
                    if (hashtagFrequencies.ContainsKey(hashtag.text.ToLower()))
                    {
                        hashtagFrequencies[hashtag.text.ToLower()] += 1;
                    }
                    else
                    {
                        hashtagFrequencies.Add(hashtag.text.ToLower(), 0);
                    }
                }
            }

            // Ordering by frequency, picking top 10 most frequent and returning result
            return hashtagFrequencies.OrderByDescending(x => x.Value)
                .Take(10)
                .Select(hashtag => new HashtagDTO {Hashtag = hashtag.Key, Occurrences = hashtag.Value});
        }

        // Additional method for when the client wishes to restart the stream with a new interval time window
        [Route("api/tweets/GetTop10Hashtags")]
        [HttpGet]
        public IEnumerable<HashtagDTO> GetTop10Hashtags(bool restartStream, double interval)
        {
            if (restartStream)
            {
                RefreshStream(interval);
            }

            return GetTop10Hashtags();
        }

        // Additional method for when the client wishes to restart the stream with the default interval time window
        [Route("api/tweets/GetTop10Hashtags")]
        [HttpGet]
        public IEnumerable<HashtagDTO> GetTop10Hashtags(bool restartStream)
        {
            return GetTop10Hashtags(restartStream, _tweetReadingInterval);
        }

        [Route("api/tweets/GetAverageTweetsPerSecond")]
        [HttpGet]
        public double GetAverageTweetsPerSecond()
        {
            // Converting "create_at" field to a DateTime object.
            var twitterDateTemplate = "ddd MMM dd HH:mm:ss +ffff yyyy";
            var firstTweetCreatedAt = DateTime.ParseExact(_tweets.Value.First().created_at, twitterDateTemplate, new System.Globalization.CultureInfo("en-US"));
            var lastTweetCreatedAt = DateTime.ParseExact(_tweets.Value.Last().created_at, twitterDateTemplate, new System.Globalization.CultureInfo("en-US"));

            // Calculating the time spanned between the first and the last tweet
            var span = (lastTweetCreatedAt - firstTweetCreatedAt);

            // Calculating the average number of tweets per second using the tweet count
            return _tweets.Value.Count / span.TotalSeconds;
        }

        // Additional method for when the client wishes to restart the stream with a new interval time window
        [Route("api/tweets/GetAverageTweetsPerSecond")]
        [HttpGet]
        public double GetAverageTweetsPerSecond(bool restartStream, double interval)
        {
            if (restartStream)
            {
                RefreshStream(interval);
            }

            return GetAverageTweetsPerSecond();
        }

        // Additional method for when the client wishes to restart the stream with the default interval time window
        [Route("api/tweets/GetAverageTweetsPerSecond")]
        [HttpGet]
        public double GetAverageTweetsPerSecond(bool restartStream)
        {
            return GetAverageTweetsPerSecond(restartStream, _tweetReadingInterval);
        }
    }
}
