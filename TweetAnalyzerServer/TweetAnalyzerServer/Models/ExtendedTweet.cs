﻿using System.Collections.Generic;

namespace TweetAnalyzerServer.Models
{
    // Classes created by converter that converts Json data to C# classes.
    public class ExtendedTweet
    {
        public string full_text { get; set; }
        public IList<int> display_text_range { get; set; }
        public Entities entities { get; set; }
    }
}