﻿using System.Collections.Generic;

namespace TweetAnalyzerServer.Models
{
    // Classes created by converter that converts Json data to C# classes.
    public class Entities
    {
        public IList<Hashtag> hashtags { get; set; }
        public IList<Url> urls { get; set; }
        public IList<object> user_mentions { get; set; }
        public IList<object> symbols { get; set; }
    }
}