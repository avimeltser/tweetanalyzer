﻿using System.Collections.Generic;

namespace TweetAnalyzerServer.Models
{
    // Classes created by converter that converts Json data to C# classes.
    public class Url
    {
        public string url { get; set; }
        public string expanded_url { get; set; }
        public string display_url { get; set; }
        public IList<int> indices { get; set; }
    }
}