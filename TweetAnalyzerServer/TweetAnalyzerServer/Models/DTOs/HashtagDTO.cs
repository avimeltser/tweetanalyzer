﻿namespace TweetAnalyzerServer.Models.DTOs
{
    public class HashtagDTO
    {
        public string Hashtag { get; set; }

        // Total occurrences of the hashtag in the tweets read
        public int Occurrences { get; set; }
    }
}