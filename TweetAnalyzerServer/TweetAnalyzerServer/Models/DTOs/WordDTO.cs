﻿namespace TweetAnalyzerServer.Models.DTOs
{
    public class WordDTO
    {
        public string Word { get; set; }

        // Total occurrences of the word in the tweets read
        public int Occurrences { get; set; }
    }
}