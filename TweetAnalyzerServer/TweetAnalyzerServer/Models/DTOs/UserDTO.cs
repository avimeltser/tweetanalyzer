﻿namespace TweetAnalyzerServer.Models.DTOs
{
    public class UserDTO
    {
        public string ScreenName { get; set; }
        
        // Total tweets posted by the user
        public int StatusCount { get; set; }
    }
}