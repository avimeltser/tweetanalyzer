﻿namespace TweetAnalyzerServer.Models
{
    // Classes created by converter that converts Json data to C# classes.
    public class QuotedStatusPermalink
    {
        public string url { get; set; }
        public string expanded { get; set; }
        public string display { get; set; }
    }
}