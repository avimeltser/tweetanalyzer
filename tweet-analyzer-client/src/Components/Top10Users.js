import React, {Component} from 'react'
import * as PropTypes from "prop-types";
import Form from "react-bootstrap/Form";
import Card from "react-bootstrap/Card";
import {serverUrl} from "../App";

class Top10Users extends Component {
    state = {
        topUsers: [],
        input: '',
    }

    constructor(props, context) {
        super(props, context);
        this.fetchTop10Users();
    }

    onChangeHandler(e){
        this.setState({
            input: e.target.value,
        })}

    render() {
        // Creating a list filtered by the text input
        const list = this.state.topUsers
            .filter(user => this.state.input === '' || user.ScreenName.includes(this.state.input))
            .map((user) =>
                <Card key={user.ScreenName}>
                    <Card.Body>
                        <Card.Title>{user.ScreenName}</Card.Title>
                        <Card.Text>Tweets: {user.StatusCount}</Card.Text>
                    </Card.Body>
                </Card>
            );

        return (
            <div>
                <h1>Top 10 Users</h1>
                <Form inline>
                    <Form.Control size="sm" type="text" placeholder="filter" value={this.state.input} onChange={this.onChangeHandler.bind(this)} />
                </Form>
                {list}
            </div>
        )
    }

    // Fetching top users data
    fetchTop10Users(){
        fetch(serverUrl + 'GetTop10Users')
            .then(res => res.json())
            .then((data) => {
                this.setState({ topUsers: data })
            })
            .catch(console.log)
    }
}

Top10Users.propTypes = {topUsers: PropTypes.any}

export default Top10Users