import React, {Component} from 'react'
import * as PropTypes from "prop-types";
import Card from "react-bootstrap/Card";
import Form from "react-bootstrap/Form";
import {serverUrl} from "../App";

class Top10Words extends Component {
    state = {
        topWords: [],
        input: ''
    }

    constructor(props, context) {
        super(props, context);
        this.fetchTop10Words();
    }

    onChangeHandler(e){
        this.setState({
            input: e.target.value,
        })}

    render() {
        // Creating a list filtered by the text input
        const list = this.state.topWords
            .filter(word => this.state.input === '' || word.Word.includes(this.state.input))
            .map((word) =>
                <Card key={word.Word}>
                    <Card.Body>
                        <Card.Title>"{word.Word}"</Card.Title>
                        <Card.Text>Count: {word.Occurrences}</Card.Text>
                    </Card.Body>
                </Card>
            );

        return (
            <div>
                <h1>Top 10 Words</h1>
                <Form inline>
                    <Form.Control size="sm" type="text" placeholder="filter" value={this.state.input} onChange={this.onChangeHandler.bind(this)} />
                </Form>
                {list}
            </div>
        )
    }

    // Fetching top words data
    fetchTop10Words(){
        fetch(serverUrl + 'GetTop10Words')
            .then(res => res.json())
            .then((data) => {
                this.setState({ topWords: data })
            })
            .catch(console.log)
    }
}

Top10Words.propTypes = {topWords: PropTypes.any}

export default Top10Words