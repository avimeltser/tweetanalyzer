import React, {Component} from 'react'
import * as PropTypes from "prop-types";
import {serverUrl} from "../App";

class AverageTweetsPerSecond extends Component {
    state = {
        averageTweetsPerSecond: 0
    }

    constructor(props, context) {
        super(props, context);
        this.GetAverageTweetsPerSecond();
    }

    render() {
        return (
            <div>
                <h1>Average Tweets Per Second</h1>
                <h5>{this.state.averageTweetsPerSecond}</h5>
            </div>
        )
    }

    // Fetching average tweets per second data
    GetAverageTweetsPerSecond(){
        fetch(serverUrl + 'GetAverageTweetsPerSecond')
            .then(res => res.json())
            .then((data) => {
                this.setState({ averageTweetsPerSecond: data })
            })
            .catch(console.log)
    }
}

AverageTweetsPerSecond.propTypes = {averageTweetsPerSecond: PropTypes.any}

export default AverageTweetsPerSecond