import React, {Component} from 'react'
import * as PropTypes from "prop-types";
import Card from "react-bootstrap/Card";
import Form from "react-bootstrap/Form";
import {serverUrl} from "../App";

class Top10Hashtags extends Component {
    state = {
        topHashtags: [],
        input: ''
    };

    constructor(props, context) {
        super(props, context);
        this.fetchTop10Words();
    }

    onChangeHandler(e){
        this.setState({
            input: e.target.value,
        })}

    render() {
        // Creating a list filtered by the text input
        const list = this.state.topHashtags
            .filter(hashtag => this.state.input === '' || hashtag.Hashtag.includes(this.state.input))
            .map((hashtag) =>
                <Card key={hashtag.Hashtag}>
                    <Card.Body>
                        <Card.Title>#{hashtag.Hashtag}</Card.Title>
                        <Card.Text>Count: {hashtag.Occurrences}</Card.Text>
                    </Card.Body>
                </Card>
            );

        return (
            <div>
                <h1>Top 10 Hashtags</h1>
                <Form inline>
                    <Form.Control size="sm" type="text" placeholder="filter" value={this.state.input} onChange={this.onChangeHandler.bind(this)} />
                </Form>
                {list}
            </div>
        )
    }

    // Fetching top hashtags data
    fetchTop10Words(){
        fetch(serverUrl + 'GetTop10Hashtags')
            .then(res => res.json())
            .then((data) => {
                this.setState({ topHashtags: data })
            })
            .catch(console.log)
    }
}

Top10Hashtags.propTypes = {topHashtags: PropTypes.any}

export default Top10Hashtags