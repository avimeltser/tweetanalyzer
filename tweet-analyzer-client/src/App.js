import React from 'react';
import '@trendmicro/react-sidenav/dist/react-sidenav.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Top10Words from './Components/Top10Words';
import {BrowserRouter as Router, Link, Route} from 'react-router-dom';
import Top10Users from "./Components/Top10Users";
import Top10Hashtags from "./Components/Top10Hashtags";
import AverageTweetsPerSecond from "./Components/AverageTweetsPerSecond";
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";

export const serverUrl = 'https://localhost:44327/api/tweets/';

function App() {

    // Setting params and methods for handling opening thed Restart Stream dialog
    const [open, setOpen] = React.useState(false);
    const handleClickRestartStream = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    // Handles submitting a request to restart the stream with a specific time interval
    const handleSubmit = () => {
        const milliseconds = document.getElementById('milliseconds').value;
        fetch(serverUrl + 'RefreshStream?interval='+ milliseconds)
            .catch(console.log);

        // Close dialog window
        setOpen(false);
    };

    return (
        <div className="App">
            <Router>
                <Paper>
                    <Tabs>
                        <Link to={'/topUsers/'}><Tab label="Top 10 Users"/></Link>
                        <Link to={'/topWords/'}><Tab label="Top 10 Words"/></Link>
                        <Link to={'/topHashtags/'}><Tab label="Top 10 Hashtags"/></Link>
                        <Link to={'/averageTweetsPerSecond/'}><Tab label="Average Tweets Per Second"/></Link>
                        <Tab onClick={handleClickRestartStream} label="Restart Stream"/>
                    </Tabs>
                </Paper>
                <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">Restart Stream</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Please enter the requested stream interval in milliseconds
                        </DialogContentText>
                        <TextField
                            autoFocus
                            margin="dense"
                            id="milliseconds"
                            fullWidth
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleClose} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={handleSubmit} color="primary">
                            Submit
                        </Button>
                    </DialogActions>
                </Dialog>
                <Route exact={true} path={'/topUsers/'}>
                    <Top10Users/>
                </Route>
                <Route exact={true} path={'/topWords/'}>
                    <Top10Words/>
                </Route>
                <Route exact={true} path={'/topHashtags/'}>
                    <Top10Hashtags/>
                </Route>
                <Route exact={true} path={'/averageTweetsPerSecond/'}>
                    <AverageTweetsPerSecond/>
                </Route>
            </Router>
        </div>
    );
}

export default App;
